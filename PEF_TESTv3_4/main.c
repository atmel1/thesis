/*------------------ References ------------------------*/
//https://community.atmel.com/projects/decimal-integer-sprintf-library
//http://www.nongnu.org/avr-libc/user-manual/group__avr__string.html
//https://stackoverflow.com/questions/3131319/how-to-correctly-assign-a-new-string-value
#define _NOP() do { __asm__ __volatile__ ("nop"); } while (0)
// used for a very short delay
/*------------------- Libraries ------------------------*/

#include <atmel_start.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "KATCON_LOGO.h"	

/*----------------- PANTALLA LCD -----------------------*/
void Send_Data(unsigned char data);
void Send_Command(unsigned char command);
void Set_GraphicMode();
void Initialize();
void Clear_Graphics();
void Fill_Graphics(char Filler);
void Image_Graphics(char * image);
void Write_String ( unsigned Y ,char * string );
void DATA_OUT(int data);

/*------------------ Pruebas ---------------------------*/
void USART_TEST();
void ADC_TEST();
void USART_TEST_PROTOCOLDATA();
void find_String_TEST();

/*------------------ ESP8266 ---------------------------*/
//#define WIFI_UDEM_NP "AT+CWJAP=\"UDEM WiFi\",\"\""
//#define WIFI_UDEM_WP "AT+CWJAP=\"UDEM_Visitantes\",\"UD3M.C4MPUS\""
//#define WIFI_CASA "AT+CWJAP=\"BRV64\",\"Hentai69\""
//#define WIFI_KATCON "AT+CWJAP=\"KATCON_GUEST\",\"1nvit4d02016kat\""
//#define ESP_OK "OK"
//#define WIFI_CONNECTED "WIFI CONNECTED"
void connect_WIFI();
void WIFI_alive();
void set_IP();

/*------------------ UBIDOTS ---------------------------*/
//Devices > Device > Variable > ID

//PROFILE > API Credentials > Tokens > Default token
char CONTENT_LENGTH[18] ="";
int con_UBIDOTS_site();
void send_UBIDOTS();
/*----------------------- APPLICATION --------------------------*/

char *strclr(char * string);
char message[16] =  "   Hello KATCON!";

adc_0_channel_t canal;
unsigned int numero = 2047;
unsigned int numeros[8];
float numero_fl = 3.1416;
char numero_str[5] = "";
char numero_strfl[10]="";


int main(void){
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	Initialize();       //Initialise GLCD
	Set_GraphicMode();
	Clear_Graphics();
	Image_Graphics(KATCON_LOGO);
	_delay_ms(2000);
	Initialize();       //Clears all Screen
	WIFI_alive();
	connect_WIFI();
	set_IP();
	Write_String(0x98, "After IP!");
	_delay_ms(5000);
	int status = 0;
	while (1) {
		//Write_String(0x98, "In Main!");
		//_delay_ms(5000);
		status = con_UBIDOTS_site();
		//Write_String(0x98, "About2SND!");
		//_delay_ms(5000);
		send_UBIDOTS();
		PORTE_toggle_pin_level(0);
		//_delay_ms(500);
	}
}

/*----------------------- UBIDOST METHODS -----------------------*/

void WIFI_alive(){
	char WIFI_RESP[14] = "              ";
	Initialize();       //Clears all Screen
	Write_String(0x80, "Checking for ESP");
	Write_String(0x90, "If no OK for 30s");
	Write_String(0x88, "Please Reset");
	USART_WIFI_write_string_CRNL("AT");
	while (strstr(WIFI_RESP,"OK") == NULL){
		USART_WIFI_read_until_CR(&WIFI_RESP[0]);
	}
	Write_String(0x99, "ESP OK");
}

void connect_WIFI(){
	char WIFI_RESP[20] = "";
	Initialize();       //Clears all Screen
	Write_String(0x80, "CON 2 WIFI");
	Write_String(0x90, "Attempting...");
	//USART_WIFI_write_string_CRNL("AT+CWJAP=\"BRV64\",\"Hentai69\"");
	//USART_WIFI_write_string_CRNL("AT+CWJAP=\"UDEM WiFi\",\"\"");
	USART_WIFI_write_string_CRNL("AT+CWJAP=\"KATCON_GUEST\",\"1nvit4d02016kat\"");
	char Attempt[16] = "";
	int nAttempt = 1;
	while(1){
		USART_WIFI_read_until_CR(&WIFI_RESP[0]);	//OK
		if (strstr(WIFI_RESP,"OK") != NULL){
			break;
		}else if (strstr(WIFI_RESP,"FAIL")){
			sprintf(Attempt,"RE-Attempt...%d",nAttempt++);
			Write_String(0x88, Attempt);
			//USART_WIFI_write_string_CRNL("AT+CWJAP=\"BRV64\",\"Hentai69\"");
			//USART_WIFI_write_string_CRNL("AT+CWJAP=\"UDEM WiFi\",\"\"");
			USART_WIFI_write_string_CRNL("AT+CWJAP=\"KATCON_GUEST\",\"1nvit4d02016kat\"");
		
		}
	}
	Write_String(0x98, "CONNECTED! :D");
}
	//Write_String(0x90, WIFI_RESP); 
	//TODO Poder agregar componentes de redundancia para guardar datos en SD y luego mandarlos por WIFI

	//CAMBIAR DORMINIO A DIRECCION IP
	//AGREGAR FUNCION PARA ASIGNAR IP

void set_IP(){
	char WIFI_RESP[30] = "";
	Initialize();       //Clears all Screen
	Write_String(0x80, "SET IP WIFI");
	Write_String(0x90, "Attempting...");
	USART_WIFI_write_string_CRNL("AT+CIPAP=\"10.0.0.97\",\"10.0.0.1\",\"255.255.255.0\"");
	char Attempt[20] = "";
	int nAttempt = 1;
	while(1){
		USART_WIFI_read_until_CR(&WIFI_RESP[0]);	//OK
		if (strstr(WIFI_RESP,"OK") != NULL){
			break;
		}else if (strstr(WIFI_RESP,"FAIL")){
			sprintf(Attempt,"RE-Attempt...%d",nAttempt++);
			Write_String(0x88, Attempt);
			USART_WIFI_write_string_CRNL("AT+CIPAP=\"10.0.0.97\",\"10.0.0.1\",\"255.255.255.0\"");
		}
	}
	Write_String(0x98, "IP SET! :D");
}

int con_UBIDOTS_site(){
	char WIFI_RESP[20] = "              ";
	Initialize();       //Clears all Screen
	Write_String(0x80, "CON 2 UBIDOTS");
	Write_String(0x90, "Attempting...");
	//USART_WIFI_write_string_CRNL("AT+CIPSTART=\"TCP\",\"things.ubidots.com\",80");	// PORT 80 Protocolo general para HTTP
	USART_WIFI_write_string_CRNL("AT+CIPSTART=\"TCP\",\"50.23.124.68\",80");	// PORT 80 Protocolo general para HTTP
	char Attempt[16] = "";
	int nAttempt = 1;
	while(1){
		//Write_String(0x98, "ProtocolSNT!");
		//_delay_ms(5000);
		USART_WIFI_read_until_CR(&WIFI_RESP[0]);	//OK
		if (strstr(WIFI_RESP,"OK")){
			//Write_String(0x98, "CON STABL!");
			//_delay_ms(5000);
			break;
		}else if (strstr(WIFI_RESP,"CLOSED")){
			Write_String(0x90, "CON CLOSED!");
			sprintf(Attempt,"RE-Attempt...%d",nAttempt++);
			Write_String(0x88, Attempt);
			//_delay_ms(3000);
			//USART_WIFI_write_string_CRNL("AT+CIPSTART=\"TCP\",\"things.ubidots.com\",80");	// PORT 80 Protocolo general para HTTP
			USART_WIFI_write_string_CRNL("AT+CIPSTART=\"TCP\",\"50.23.124.68\",80");
		}else if (strstr(WIFI_RESP,"ALREADY")){
			Write_String(0x90, "ALREADY CON!");
			//_delay_ms(3000);
			break;
		}else if (strstr(WIFI_RESP,"FAIL")){
			Write_String(0x90, "FAILES 2 CON!");
			sprintf(Attempt,"RE-Attempt...%d",nAttempt++);
			Write_String(0x88, Attempt);
			//_delay_ms(3000);
			//USART_WIFI_write_string_CRNL("AT+CIPSTART=\"TCP\",\"things.ubidots.com\",80");	// PORT 80 Protocolo general para HTTP
			USART_WIFI_write_string_CRNL("AT+CIPSTART=\"TCP\",\"50.23.124.68\",80");
		}
	}
	//strcpy(WIFI_RESP,"TAQUITOS641024");
	Write_String(0x98, "Hola UBIDOTS! :D");
	//_delay_ms(5000);
	return 0;
}

void send_UBIDOTS(){
	Initialize();       //Clears all Screen
	char WIFI_RESP[500];
	int data[16];
	Write_String(0x80, "DATA 2 UBIDOTS");
	for (int i = 0; i < 16; i++){
		if (i < 8){
			data[i] = ADC_0_get_conversion(i);
		} 
		else{
			data[i] = ADC_1_get_conversion(i & 0b00000111);
		}
	}
	Write_String(0x90, "Attempting...");
	char AT_CIPSEND[14] ="";
	char CONTENT[271] = "";
	char MotorData[18] = "";
	//sprintf(AT_CIPSEND,"AT+CIPSEND=%d",204);
	sprintf(AT_CIPSEND,"AT+CIPSEND=%d",458);
	USART_WIFI_write_string_CRNL(AT_CIPSEND);
	while(1){
		USART_WIFI_read_until_CR(&WIFI_RESP[0]);	// Wait for "OK" This allows you to send data
		if (strstr(WIFI_RESP,"OK")  != NULL){
			break;
		}
	}
	_delay_ms(1);
	Write_String(0x88, "RDY 4 ACTION!");
	USART_WIFI_write_string_CRNL("POST /api/v1.6/devices/robot1_katcon/?token=me2SCNBjsKDE4htF5vIjF6FOSuGkPm HTTP/1.1");
	USART_WIFI_write_string_CRNL("Host: things.ubidots.com");	// HOST
	USART_WIFI_write_string_CRNL("Content-Type: application/json");	// Content Type
	sprintf(CONTENT_LENGTH,"Content-Length: %d",271);
	USART_WIFI_write_string_CRNL(CONTENT_LENGTH);
	USART_WIFI_write_string_CRNL("Connection: close");	// CON
	USART_WIFI_write_string_CRNL("");
	strcat(CONTENT,"{");
	sprintf(MotorData,"\"motor_14\": \"%4u\"",data[1]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_13\": \"%4u\"",data[2]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_12\": \"%4u\"",data[3]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_11\": \"%4u\"",data[4]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_10\": \"%4u\"",data[5]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_9\": \"%4u\"",data[6]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_8\": \"%4u\"",data[7]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_7\": \"%4u\"",data[9]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_6\": \"%4u\"",data[10]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_5\": \"%4u\"",data[11]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_4\": \"%4u\"",data[12]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_3\": \"%4u\"",data[13]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_2\": \"%4u\"",data[14]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,", ");
	sprintf(MotorData,"\"motor_1\": \"%4u\"",data[15]);
	strcat(CONTENT,MotorData);
	strcat(CONTENT,"}");
	USART_WIFI_write_string_CRNL(CONTENT);
	USART_WIFI_write_string_CRNL("");
	while(1){
		USART_WIFI_read_until_CR(&WIFI_RESP[0]);
		/*if (strstr(WIFI_RESP,"OK") != NULL){
			ok = 1;
		}else
		*/
		if (strstr(WIFI_RESP,"CLOSED") != NULL){
			//Write_String(0x88, "CON CLOSED");
			//con_UBIDOTS_site();
			//_delay_ms(5000);
			break;
		}else if (strstr(WIFI_RESP,"FAIL") != NULL){
			Write_String(0x88, "SEND FAILED?");
		}else if (strstr(WIFI_RESP,"ERROR") != NULL){
			Write_String(0x88, "GOT ERROR?");
		}else if (strstr(WIFI_RESP,"") != NULL){
		}
		/*
		else{
			Write_String(0x88, "Unkown ERROR");
			USART_WIFI_write_string_CRNL("AT+RST");
			_delay_ms(5000);
			connect_WIFI();
			ok++;
		}
		*/
	}
	Write_String(0x98, "Data SENT! :D");
	//strcpy(WIFI_RESP,"TAQUITOS641024");
}


/*----------------------- TEST METHODS -------------------------*/

void USART_TEST(){
	for (int i = 64; i <= 126; i++)
		{
			USART_WIFI_write(i);
			USART_OTC_write(i);
		}
		USART_WIFI_write(13);
		USART_OTC_write(13);
		USART_WIFI_write(10);
		USART_OTC_write(10);
}

void ADC_TEST(){
		Initialize();       //Clears all Screen

		numeros[0] = ADC_0_get_conversion(0);
		sprintf(numero_str,"S1:%4d",numeros[0]);
		Write_String(0x80, numero_str);
		
		numeros[1] = ADC_0_get_conversion(1);
		sprintf(numero_str,"S2:%4d",numeros[1]);
		Write_String(0x90, numero_str);

		numeros[2] = ADC_0_get_conversion(2);
		sprintf(numero_str,"S3:%4d",numeros[2]);
		Write_String(0x88, numero_str);

		numeros[3] = ADC_0_get_conversion(3);
		sprintf(numero_str,"S4:%4d",numeros[3]);
		Write_String(0x98, numero_str);
		_delay_ms(500);

		Initialize();       //Clears all Screen
		
		numeros[4] = ADC_0_get_conversion(4);
		sprintf(numero_str,"S5:%4d",numeros[4]);
		Write_String(0x80, numero_str);
		
		numeros[5] = ADC_0_get_conversion(5);
		sprintf(numero_str,"S6:%4d",numeros[5]);
		Write_String(0x90, numero_str);

		numeros[6] = ADC_0_get_conversion(6);
		sprintf(numero_str,"S7:%4d",numeros[6]);
		Write_String(0x88, numero_str);

		numeros[7] = ADC_0_get_conversion(7);
		sprintf(numero_str,"S8:%4d",numeros[7]);
		Write_String(0x98, numero_str);
		_delay_ms(500);
		//Image_Graphics(MH);
		
		Initialize();       //Clears all Screen

		numeros[0] = ADC_1_get_conversion(0);
		sprintf(numero_str,"S1:%4d",numeros[0]);
		Write_String(0x80, numero_str);
		
		numeros[1] = ADC_1_get_conversion(1);
		sprintf(numero_str,"S2:%4d",numeros[1]);
		Write_String(0x90, numero_str);

		numeros[2] = ADC_1_get_conversion(2);
		sprintf(numero_str,"S3:%4d",numeros[2]);
		Write_String(0x88, numero_str);

		numeros[3] = ADC_1_get_conversion(3);
		sprintf(numero_str,"S4:%4d",numeros[3]);
		Write_String(0x98, numero_str);
		_delay_ms(500);

		Initialize();       //Clears all Screen
		
		numeros[4] = ADC_1_get_conversion(4);
		sprintf(numero_str,"S5:%4d",numeros[4]);
		Write_String(0x80, numero_str);
		
		numeros[5] = ADC_1_get_conversion(5);
		sprintf(numero_str,"S6:%4d",numeros[5]);
		Write_String(0x90, numero_str);

		numeros[6] = ADC_1_get_conversion(6);
		sprintf(numero_str,"S7:%4d",numeros[6]);
		Write_String(0x88, numero_str);

		numeros[7] = ADC_1_get_conversion(7);
		sprintf(numero_str,"S8:%4d",numeros[7]);
		Write_String(0x98, numero_str);
		_delay_ms(500);
}

void USART_TEST_PROTOCOLDATA(){
	char CONNECT_UBIDOTS[] = "AT+CIPSTART=\"TCP\",\"things.ubidots.com\",80";
	char VAR_ID_Temperature[] = "58c25271762542150b669169";
	char TOKEN_BRV64[] = "me2SCNBjsKDE4htF5vIjF6FOSuGkPm";
	char POST_COM[223] = "";
	char AT_CIPSEND[14] ="";
	char CONTENT[17] = "";
	char HOST_UBIDOTS[] = "Host: things.ubidots.com";
	char CONTENT_TYPE[] = "Content-Type: application/json";
	USART_WIFI_write_string_CRNL("AT+CWJAP=\"UDEM WiFi\",\"\"");
	USART_WIFI_write_string_CRNL(CONNECT_UBIDOTS);
	sprintf(AT_CIPSEND,"AT+CIPSEND=%d",204);
	USART_WIFI_write_string_CRNL(AT_CIPSEND);
	strcat(POST_COM,"POST /api/v1.6/variables/");
	strcat(POST_COM,VAR_ID_Temperature);
	strcat(POST_COM,"/values/?token=");
	strcat(POST_COM,TOKEN_BRV64);
	strcat(POST_COM," HTTP/1.1");
	USART_WIFI_write_string_CRNL(POST_COM);
	USART_WIFI_write_string_CRNL(HOST_UBIDOTS);
	USART_WIFI_write_string_CRNL(CONTENT_TYPE);
	sprintf(CONTENT_LENGTH,"Content-Length: %d",17);
	USART_WIFI_write_string_CRNL(CONTENT_LENGTH);
	USART_WIFI_write_string_CRNL("");
	sprintf(CONTENT,"{\"value\": \"%4u\"}",numero);
	USART_WIFI_write_string_CRNL(CONTENT);
	USART_WIFI_write_string_CRNL("");
}

void find_String_TEST(){
	if (strstr("HOLAOK\n","OK") != NULL){
		//USART_WIFI_write_string_CRNL("Found The OK");
		Write_String(0x88, "Found OK");
	}
}

/*--------------------LCD METHODS ------------------------------*/

void Initialize(){
	//_delay_ms(100);
	RS_LCD_set_level(false); //RS=0;
	RW_LCD_set_level(false); //RW=0;
	_delay_us(400);
	RST_LCD_set_level(true); //RST = 1;
	_delay_ms(10);             // Short delay after resetting.
	Send_Command(0b00110000) ;  // 8-bit mode.
	_delay_us(100);
	Send_Command(0b00110000) ;  // 8-bit mode again.
	_delay_us(110);
	Send_Command(0b00001100);   // display on
	_delay_us(100);
	Send_Command(0b00000001);   // Clears screen.
	_delay_ms(2);
	Send_Command(0b00000110);   // Cursor moves right, no display shift.
	_delay_us(80);
	Send_Command(0b00000010);   // Returns to home. Cursor moves to starting point.
}

//========= Sends data to PINs
void DATA_OUT(int data){
	DATA_LCD0_set_level(data & 0b00000001);
	data = data >> 1;
	DATA_LCD1_set_level(data & 0b00000001);
	data = data >> 1;
	DATA_LCD2_set_level(data & 0b00000001);
	data = data >> 1;
	DATA_LCD3_set_level(data & 0b00000001);
	data = data >> 1;
	DATA_LCD4_set_level(data & 0b00000001);
	data = data >> 1;
	DATA_LCD5_set_level(data & 0b00000001);
	data = data >> 1;
	DATA_LCD6_set_level(data & 0b00000001);
	data = data >> 1;
	DATA_LCD7_set_level(data & 0b00000001);
}

//========= Setting the control lines to send a Command to the data bus ================
void Send_Command(unsigned char command){
	RW_LCD_set_level(false); //RW=0;
	RS_LCD_set_level(false); //RS = 0;
	_delay_us(20);
	E_LCD_set_level(true); //EN = 1;
	DATA_OUT(command);
	_delay_us(50);
	E_LCD_set_level(false); //EN = 0;
}

//============= Setting the control lines to send Data to the data bus =====================
void Send_Data(unsigned char data){
	RS_LCD_set_level(true); //RS = 1;
	_delay_us(40);
	DATA_OUT(data);
	_delay_us(30);
	E_LCD_set_level(true); //EN = 1;
	_delay_us(20);
	E_LCD_set_level(false); //EN = 0;
	_delay_us(20);
}

//======================= Sent Command to set Extanded mode ====================
void Set_GraphicMode(){
	Send_Command(0b00110100); // Extended instuction set, 8bit
	_delay_us(100);
	Send_Command(0b00110110); // Repeat instrution with bit1 set
	_delay_us(100);
}

//=========== This function set all the pixels to off in the graphic controller =================
void Clear_Graphics(){
	unsigned char x, y;
	for(y = 0; y < 64; y++){
		if(y < 32){
			Send_Command(0x80 | y);
			Send_Command(0x80);
		}
		else{
			Send_Command(0x80 | (y-32));
			Send_Command(0x88);
		}
		for(x = 0; x < 16; x++){
			Send_Data(0x00);
		}
	}
}
//=========== This function set all the pixels to a value in the graphic controller =================
void Fill_Graphics(char Filler){
	unsigned char x, y;
	for(y = 0; y < 64; y++){
		if(y < 32){
			Send_Command(0x80 | y);
			Send_Command(0x80);
		}
		else{
			Send_Command(0x80 | (y-32));
			Send_Command(0x88);
		}
		for(x = 0; x < 16; x++){
			Send_Data(Filler);
		}
	}
}
//=========== This function set all the pixels to off in the graphic controller =================
void Image_Graphics(char * image){
	unsigned char x, y;
	for(y = 0; y < 64; y++){
		if(y < 32){
			Send_Command(0x80 | y);
			Send_Command(0x80);
		}
		else{
			Send_Command(0x80 | (y-32));
			Send_Command(0x88);
		}
		for(x = 0; x < 16; x++){
			Send_Data(*image++);
		}
	}
}
//==== Send one character at the time from the 'message' string ===========
void Write_String ( unsigned Y ,char * string ){
	Send_Command(Y);
	while(*string!= '\0'){      // Looking for code signigying 'end of line' .
		Send_Data(*string++);
	}
}
/*
char *strclr(char * string){
	for (int i = 0; i < strlen(string); i++){
		string[i] = " ";
	}
	return string;
}
*/