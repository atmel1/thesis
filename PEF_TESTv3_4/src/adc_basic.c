/**
 * \file
 *
 * \brief ADC Basic driver implementation.
 *
 *
 * Copyright (C) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 *
 */

#include <adc_basic.h>

adc_irq_cb_t ADC_0_cb = NULL; /**
* \brief Initialize ADC interface
*/
int8_t ADC_0_init()
{

	// ADCA.CAL = 0x0; /* Calibration Value: 0x0 */

	// ADCA.CH0.CTRL = ADC_CH_GAIN_1X_gc /* 1x gain */
	//		 | ADC_CH_INPUTMODE_INTERNAL_gc; /* Internal inputs, no gain */

	// ADCA.CH0.MUXCTRL = ADC_CH_MUXINT_TEMP_gc /* Temperature Reference */
	//		 | ADC_CH_MUXNEG_PIN0_gc; /* Input pin 0 (Input Mode = 2) */

	ADCA.CH0.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc /* Interrupt on conversion complete */
	                   | ADC_CH_INTLVL_HI_gc;     /* High level */

	// ADCA.CH0.SCAN = 0x0 << ADC_CH_SCANNUM_gp /* Number of Channels included in scan: 0x0 */;

	// ADCA.CH1.CTRL = ADC_CH_GAIN_1X_gc /* 1x gain */
	//		 | ADC_CH_INPUTMODE_INTERNAL_gc; /* Internal inputs, no gain */

	// ADCA.CH1.MUXCTRL = ADC_CH_MUXINT_TEMP_gc /* Temperature Reference */
	//		 | ADC_CH_MUXNEG_PIN0_gc; /* Input pin 0 (Input Mode = 2) */

	// ADCA.CH1.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc /* Interrupt on conversion complete */;

	// ADCA.CH2.CTRL = ADC_CH_GAIN_1X_gc /* 1x gain */
	//		 | ADC_CH_INPUTMODE_INTERNAL_gc; /* Internal inputs, no gain */

	// ADCA.CH2.MUXCTRL = ADC_CH_MUXINT_TEMP_gc /* Temperature Reference */
	//		 | ADC_CH_MUXNEG_PIN0_gc; /* Input pin 0 (Input Mode = 2) */

	// ADCA.CH2.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc /* Interrupt on conversion complete */;

	// ADCA.CH3.CTRL = ADC_CH_GAIN_1X_gc /* 1x gain */
	//		 | ADC_CH_INPUTMODE_INTERNAL_gc; /* Internal inputs, no gain */

	// ADCA.CH3.MUXCTRL = ADC_CH_MUXINT_TEMP_gc /* Temperature Reference */
	//		 | ADC_CH_MUXNEG_PIN0_gc; /* Input pin 0 (Input Mode = 2) */

	// ADCA.CH3.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc /* Interrupt on conversion complete */;

	ADCA.CTRLB = //0 << ADC_IMPMODE_bp        /* Gain Stage Impedance Mode: disabled */
	             //| ADC_CURRLIMIT_NO_gc      /* No limit */
	             //| 0 << ADC_CONMODE_bp      /* Conversion Mode: disabled */
				   0 << ADC_CONMODE_bp      /* Conversion Mode: disabled */
	             | 1 << ADC_FREERUN_bp      /* Free Running Mode Enable: enabled */
	             | ADC_RESOLUTION_12BIT_gc; /* 12-bit right-adjusted result */

	// ADCA.EVCTRL = ADC_SWEEP_0_gc /* ADC Channel 0 */
	//		 | ADC_EVSEL_0123_gc /* Event Channel 0,1,2,3 */
	//		 | ADC_EVACT_NONE_gc; /* No event action */

	// ADCA.REFCTRL = ADC_REFSEL_INT1V_gc /* Internal 1V */
	//		 | 0 << ADC_BANDGAP_bp /* Bandgap enable: disabled */
	//		 | 0 << ADC_TEMPREF_bp; /* Temperature Reference Enable: disabled */

	ADCA.PRESCALER = ADC_PRESCALER_DIV32_gc; /* Divide clock by 32 */

	// ADCA.CMP = 0x0; /* Compare Value: 0x0 */

	ADCA.CTRLA = 0 << ADC_FLUSH_bp    /* Flush Pipeline: disabled */
	             | 1 << ADC_ENABLE_bp /* Enable ADC: enabled */
	             | ADC_DMASEL_OFF_gc; /* Combined DMA request OFF */

	return 0;
}

void ADC_0_enable()
{
	ADCA.CTRLA |= ADC_ENABLE_bm;
}

void ADC_0_disable()
{
	ADCA.CTRLA &= ~ADC_ENABLE_bm;
}

void ADC_0_start_conversion(adc_0_channel_t channel)
{
	ADCA.CH0.MUXCTRL = channel << ADC_CH_MUXPOS_gp;
	ADCA.CH0.CTRL    = ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH0.CTRL |= ADC_CH_START_bm;
}

bool ADC_0_is_conversion_done()
{
	return (ADCA.INTFLAGS & ADC_CH0IF_bm);
}

adc_result_t ADC_0_get_conversion_result(void)
{
	return (ADCA.CH0RES);
}

adc_result_t ADC_0_get_conversion(adc_0_channel_t channel)
{
	adc_result_t res;

	ADC_0_start_conversion(channel);
	while (!ADC_0_is_conversion_done())
		;
	res           = ADC_0_get_conversion_result();
	ADCA.INTFLAGS = ADC_CH0IF_bm;
	return res;
}

uint8_t ADC_0_get_resolution()
{
	return (ADCA.CTRLA & ADC_RESOLUTION_gm) ? 12 : 8;
}

void ADC_0_register_callback(adc_irq_cb_t f)
{
	ADC_0_cb = f;
}

ISR(ADCA_CH0_vect)
{
	// Clear the interrupt flag
	ADCA.INTFLAGS = ADC_CH0IF_bm;

	if (ADC_0_cb != NULL) {
		ADC_0_cb();
	}
}

adc_irq_cb_t ADC_1_cb = NULL; /**
* \brief Initialize ADC interface
*/
int8_t ADC_1_init()
{

	// ADCB.CAL = 0x0; /* Calibration Value: 0x0 */

	// ADCB.CH0.CTRL = ADC_CH_GAIN_1X_gc /* 1x gain */
	//		 | ADC_CH_INPUTMODE_INTERNAL_gc; /* Internal inputs, no gain */

	// ADCB.CH0.MUXCTRL = ADC_CH_MUXINT_TEMP_gc /* Temperature Reference */
	//		 | ADC_CH_MUXNEG_PIN0_gc; /* Input pin 0 (Input Mode = 2) */

	ADCB.CH0.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc /* Interrupt on conversion complete */
	                   | ADC_CH_INTLVL_HI_gc;     /* High level */

	// ADCB.CH0.SCAN = 0x0 << ADC_CH_SCANNUM_gp /* Number of Channels included in scan: 0x0 */;

	// ADCB.CH1.CTRL = ADC_CH_GAIN_1X_gc /* 1x gain */
	//		 | ADC_CH_INPUTMODE_INTERNAL_gc; /* Internal inputs, no gain */

	// ADCB.CH1.MUXCTRL = ADC_CH_MUXINT_TEMP_gc /* Temperature Reference */
	//		 | ADC_CH_MUXNEG_PIN0_gc; /* Input pin 0 (Input Mode = 2) */

	// ADCB.CH1.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc /* Interrupt on conversion complete */;

	// ADCB.CH2.CTRL = ADC_CH_GAIN_1X_gc /* 1x gain */
	//		 | ADC_CH_INPUTMODE_INTERNAL_gc; /* Internal inputs, no gain */

	// ADCB.CH2.MUXCTRL = ADC_CH_MUXINT_TEMP_gc /* Temperature Reference */
	//		 | ADC_CH_MUXNEG_PIN0_gc; /* Input pin 0 (Input Mode = 2) */

	// ADCB.CH2.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc /* Interrupt on conversion complete */;

	// ADCB.CH3.CTRL = ADC_CH_GAIN_1X_gc /* 1x gain */
	//		 | ADC_CH_INPUTMODE_INTERNAL_gc; /* Internal inputs, no gain */

	// ADCB.CH3.MUXCTRL = ADC_CH_MUXINT_TEMP_gc /* Temperature Reference */
	//		 | ADC_CH_MUXNEG_PIN0_gc; /* Input pin 0 (Input Mode = 2) */

	// ADCB.CH3.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc /* Interrupt on conversion complete */;

	// ADCB.CTRLB = 0 << ADC_IMPMODE_bp /* Gain Stage Impedance Mode: disabled */
	//		 | ADC_CURRLIMIT_NO_gc /* No limit */
	//		 | 0 << ADC_CONMODE_bp /* Conversion Mode: disabled */
	//		 | 0 << ADC_FREERUN_bp /* Free Running Mode Enable: disabled */
	//		 | ADC_RESOLUTION_12BIT_gc; /* 12-bit right-adjusted result */

	// ADCB.EVCTRL = ADC_SWEEP_0_gc /* ADC Channel 0 */
	//		 | ADC_EVSEL_0123_gc /* Event Channel 0,1,2,3 */
	//		 | ADC_EVACT_NONE_gc; /* No event action */

	// ADCB.REFCTRL = ADC_REFSEL_INT1V_gc /* Internal 1V */
	//		 | 0 << ADC_BANDGAP_bp /* Bandgap enable: disabled */
	//		 | 0 << ADC_TEMPREF_bp; /* Temperature Reference Enable: disabled */

	ADCB.PRESCALER = ADC_PRESCALER_DIV32_gc; /* Divide clock by 32 */

	// ADCB.CMP = 0x0; /* Compare Value: 0x0 */

	ADCB.CTRLA = 0 << ADC_FLUSH_bp    /* Flush Pipeline: disabled */
	             | 1 << ADC_ENABLE_bp /* Enable ADC: enabled */
	             | ADC_DMASEL_OFF_gc; /* Combined DMA request OFF */

	return 0;
}

void ADC_1_enable()
{
	ADCB.CTRLA |= ADC_ENABLE_bm;
}

void ADC_1_disable()
{
	ADCB.CTRLA &= ~ADC_ENABLE_bm;
}

void ADC_1_start_conversion(adc_1_channel_t channel)
{
	ADCB.CH0.MUXCTRL = channel << ADC_CH_MUXPOS_gp;
	ADCB.CH0.CTRL    = ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCB.CH0.CTRL |= ADC_CH_START_bm;
}

bool ADC_1_is_conversion_done()
{
	return (ADCB.INTFLAGS & ADC_CH0IF_bm);
}

adc_result_t ADC_1_get_conversion_result(void)
{
	return (ADCB.CH0RES);
}

adc_result_t ADC_1_get_conversion(adc_1_channel_t channel)
{
	adc_result_t res;

	ADC_1_start_conversion(channel);
	while (!ADC_1_is_conversion_done())
		;
	res           = ADC_1_get_conversion_result();
	ADCB.INTFLAGS = ADC_CH0IF_bm;
	return res;
}

uint8_t ADC_1_get_resolution()
{
	return (ADCB.CTRLA & ADC_RESOLUTION_gm) ? 12 : 8;
}

void ADC_1_register_callback(adc_irq_cb_t f)
{
	ADC_1_cb = f;
}

ISR(ADCB_CH0_vect)
{
	// Clear the interrupt flag
	ADCB.INTFLAGS = ADC_CH0IF_bm;

	if (ADC_1_cb != NULL) {
		ADC_1_cb();
	}
}
