/**
 * \file
 *
 * \brief USART basic driver.
 *
 *
 * Copyright (C) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 *
 */
#include <compiler.h>
#include <clock_config.h>
#include <usart_basic.h>
#include <atomic.h>

#include <stdio.h>

#if defined(__GNUC__)

int USART_WIFI_printCHAR(char character, FILE *stream)
{
	USART_WIFI_write(character);
	return 0;
}

FILE USART_WIFI_stream = FDEV_SETUP_STREAM(USART_WIFI_printCHAR, NULL, _FDEV_SETUP_WRITE);

#elif defined(__ICCAVR__)

int putchar(int outChar)
{
	USART_0_write(outChar);
	return outChar;
}
#endif

int8_t USART_WIFI_init()
{

	int8_t   exp;
	uint32_t div;
	uint32_t limit;
	uint32_t ratio;
	uint32_t min_rate;
	uint32_t max_rate;
	uint32_t cpu_hz = F_CPU;
	uint32_t baud   = 115200 * 2;

	/*
	 * Check if the hardware supports the given baud rate
	 */
	/* 8 = (2^0) * 8 * (2^0) = (2^BSCALE_MIN) * 8 * (BSEL_MIN) */
	max_rate = cpu_hz / 8;
	/* 4194304 = (2^7) * 8 * (2^12) = (2^BSCALE_MAX) * 8 * (BSEL_MAX+1) */
	min_rate = cpu_hz / 4194304;

	if ((baud > max_rate) || (baud < min_rate)) {
		/* the hardware doesn't supports the given baud rate */
		return -1;
	}

	/* Find the lowest possible exponent. */
	limit = 0xfffU >> 4;
	ratio = cpu_hz / baud;

	for (exp = -7; exp < 7; exp++) {
		if (ratio < limit) {
			break;
		}

		limit <<= 1;

		if (exp < -3) {
			limit |= 1;
		}
	}

	/*
	 * Depending on the value of exp, scale either the input frequency or
	 * the target baud rate. By always scaling upwards, we never introduce
	 * any additional inaccuracy.
	 *
	 * We are including the final divide-by-8 (aka. right-shift-by-3) in
	 * this operation as it ensures that we never exceeed 2**32 at any
	 * point.
	 *
	 * The formula for calculating BSEL is slightly different when exp is
	 * negative than it is when exp is positive.
	 */
	if (exp < 0) {
		/* We are supposed to subtract 1, then apply BSCALE. We want to
		 * apply BSCALE first, so we need to turn everything inside the
		 * parenthesis into a single fractional expression.
		 */
		cpu_hz -= 8 * baud;

		/* If we end up with a left-shift after taking the final
		 * divide-by-8 into account, do the shift before the divide.
		 * Otherwise, left-shift the denominator instead (effectively
		 * resulting in an overall right shift.)
		 */
		if (exp <= -3) {
			div = ((cpu_hz << (-exp - 3)) + baud / 2) / baud;
		} else {
			baud <<= exp + 3;
			div = (cpu_hz + baud / 2) / baud;
		}
	} else {
		/* We will always do a right shift in this case, but we need to
		 * shift three extra positions because of the divide-by-8.
		 */
		baud <<= exp + 3;
		div = (cpu_hz + baud / 2) / baud - 1;
	}

	USARTC0.BAUDCTRLB = (uint8_t)(((div >> 8) & 0X0F) | (exp << 4));
	USARTC0.BAUDCTRLA = (uint8_t)div;

	// USARTC0.CTRLC = USART_PMODE_DISABLED_gc /* No Parity */
	//		 | 0 << USART_SBMODE_bp /* Stop Bit Mode: disabled */
	//		 | USART_CHSIZE_8BIT_gc /* Character size: 8 bit */
	//		 | USART_CMODE_ASYNCHRONOUS_gc; /* Async Polled Mode */

	USARTC0.CTRLB = 0 << USART_MPCM_bp    /* Multi-processor Communication Mode: disabled */
	                | 1 << USART_RXEN_bp  /* Receiver Enable: enabled */
	                | 1 << USART_TXEN_bp; /* Transmitter Enable: enabled */

#if defined(__GNUC__)
	stdout = &USART_WIFI_stream;
#endif

	return 0;
}

void USART_WIFI_enable()
{
	USARTC0.CTRLB |= USART_RXEN_bm | USART_TXEN_bm;
}

void USART_WIFI_enable_rx()
{
	USARTC0.CTRLB |= USART_RXEN_bm;
}

void USART_WIFI_enable_tx()
{
	USARTC0.CTRLB |= USART_TXEN_bm;
}

void USART_WIFI_disable()
{
	USARTC0.CTRLB &= ~(USART_RXEN_bm | USART_TXEN_bm);
}

uint8_t USART_WIFI_get_data()
{
	return USARTC0.DATA;
}

bool USART_WIFI_is_tx_ready()
{
	return (USARTC0.STATUS & USART_DREIF_bm);
}

bool USART_WIFI_is_rx_ready()
{
	return (USARTC0.STATUS & USART_RXCIF_bm);
}

bool USART_WIFI_is_tx_busy()
{
	return (!(USARTC0.STATUS & USART_TXCIF_bm));
}

uint8_t USART_WIFI_read()
{
	while (!(USARTC0.STATUS & USART_RXCIF_bm))
		;
	return USARTC0.DATA;
}

void USART_WIFI_read_until_CR(unsigned char *prt){
	while((*prt = USART_WIFI_read()) != 0x0D){ //Carrie Return
		prt++;
	}
	*prt = 0;
}

void USART_WIFI_read_until_N_CHAR(unsigned char *prt, int N){
	for (int i = 0; i < N; i++){
		*prt = USART_WIFI_read();
		prt++;
	}
	*prt = 0;
}

void USART_WIFI_write(const uint8_t data)
{
	while (!(USARTC0.STATUS & USART_DREIF_bm))
		;
	USARTC0.DATA = data;
}

void USART_WIFI_write_string(unsigned char data[]){
	int i =0;
	while (data[i] != 0x00)
	{
		USART_WIFI_write(data[i]);
		i++;
	}
}

void USART_WIFI_write_string_CRNL(unsigned char data[]){
	USART_WIFI_write_string(data);
	//USART_WIFI_write(11);
	USART_WIFI_write(0x0D);	//CR
	USART_WIFI_write(0x0A);	//NL
}

#include <stdio.h>

#if defined(__GNUC__)

int USART_OTC_printCHAR(char character, FILE *stream)
{
	USART_OTC_write(character);
	return 0;
}

FILE USART_OTC_stream = FDEV_SETUP_STREAM(USART_OTC_printCHAR, NULL, _FDEV_SETUP_WRITE);

#elif defined(__ICCAVR__)

int putchar(int outChar)
{
	USART_0_write(outChar);
	return outChar;
}
#endif

int8_t USART_OTC_init()
{

	int8_t   exp;
	uint32_t div;
	uint32_t limit;
	uint32_t ratio;
	uint32_t min_rate;
	uint32_t max_rate;
	uint32_t cpu_hz = F_CPU;
	uint32_t baud   = 19200;

	/*
	 * Check if the hardware supports the given baud rate
	 */
	/* 8 = (2^0) * 8 * (2^0) = (2^BSCALE_MIN) * 8 * (BSEL_MIN) */
	max_rate = cpu_hz / 8;
	/* 4194304 = (2^7) * 8 * (2^12) = (2^BSCALE_MAX) * 8 * (BSEL_MAX+1) */
	min_rate = cpu_hz / 4194304;

	if ((baud > max_rate) || (baud < min_rate)) {
		/* the hardware doesn't supports the given baud rate */
		return -1;
	}

	/* Find the lowest possible exponent. */
	limit = 0xfffU >> 4;
	ratio = cpu_hz / baud;

	for (exp = -7; exp < 7; exp++) {
		if (ratio < limit) {
			break;
		}

		limit <<= 1;

		if (exp < -3) {
			limit |= 1;
		}
	}

	/*
	 * Depending on the value of exp, scale either the input frequency or
	 * the target baud rate. By always scaling upwards, we never introduce
	 * any additional inaccuracy.
	 *
	 * We are including the final divide-by-8 (aka. right-shift-by-3) in
	 * this operation as it ensures that we never exceeed 2**32 at any
	 * point.
	 *
	 * The formula for calculating BSEL is slightly different when exp is
	 * negative than it is when exp is positive.
	 */
	if (exp < 0) {
		/* We are supposed to subtract 1, then apply BSCALE. We want to
		 * apply BSCALE first, so we need to turn everything inside the
		 * parenthesis into a single fractional expression.
		 */
		cpu_hz -= 8 * baud;

		/* If we end up with a left-shift after taking the final
		 * divide-by-8 into account, do the shift before the divide.
		 * Otherwise, left-shift the denominator instead (effectively
		 * resulting in an overall right shift.)
		 */
		if (exp <= -3) {
			div = ((cpu_hz << (-exp - 3)) + baud / 2) / baud;
		} else {
			baud <<= exp + 3;
			div = (cpu_hz + baud / 2) / baud;
		}
	} else {
		/* We will always do a right shift in this case, but we need to
		 * shift three extra positions because of the divide-by-8.
		 */
		baud <<= exp + 3;
		div = (cpu_hz + baud / 2) / baud - 1;
	}

	USARTE0.BAUDCTRLB = (uint8_t)(((div >> 8) & 0X0F) | (exp << 4));
	USARTE0.BAUDCTRLA = (uint8_t)div;

	// USARTE0.CTRLC = USART_PMODE_DISABLED_gc /* No Parity */
	//		 | 0 << USART_SBMODE_bp /* Stop Bit Mode: disabled */
	//		 | USART_CHSIZE_8BIT_gc /* Character size: 8 bit */
	//		 | USART_CMODE_ASYNCHRONOUS_gc; /* Async Polled Mode */

	USARTE0.CTRLB = 0 << USART_MPCM_bp    /* Multi-processor Communication Mode: disabled */
	                | 1 << USART_RXEN_bp  /* Receiver Enable: enabled */
	                | 1 << USART_TXEN_bp; /* Transmitter Enable: enabled */

#if defined(__GNUC__)
	stdout = &USART_OTC_stream;
#endif

	return 0;
}

void USART_OTC_enable()
{
	USARTE0.CTRLB |= USART_RXEN_bm | USART_TXEN_bm;
}

void USART_OTC_enable_rx()
{
	USARTE0.CTRLB |= USART_RXEN_bm;
}

void USART_OTC_enable_tx()
{
	USARTE0.CTRLB |= USART_TXEN_bm;
}

void USART_OTC_disable()
{
	USARTE0.CTRLB &= ~(USART_RXEN_bm | USART_TXEN_bm);
}

uint8_t USART_OTC_get_data()
{
	return USARTE0.DATA;
}

bool USART_OTC_is_tx_ready()
{
	return (USARTE0.STATUS & USART_DREIF_bm);
}

bool USART_OTC_is_rx_ready()
{
	return (USARTE0.STATUS & USART_RXCIF_bm);
}

bool USART_OTC_is_tx_busy()
{
	return (!(USARTE0.STATUS & USART_TXCIF_bm));
}

uint8_t USART_OTC_read()
{
	while (!(USARTE0.STATUS & USART_RXCIF_bm))
		;
	return USARTE0.DATA;
}

void USART_OTC_write(const uint8_t data)
{
	while (!(USARTE0.STATUS & USART_DREIF_bm))
		;
	USARTE0.DATA = data;
}
